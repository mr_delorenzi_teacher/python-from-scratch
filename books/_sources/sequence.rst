=======================
Sequences
=======================

.. qnum::
   :prefix: Sequence-
   :start: 2

Big Question
------------
* What is a sequence?

Key words
---------
* Sequence

Sequences (Task 1)
------------------

First a simple sequence. It does one thing after another.
Run this one line at a time, by tapping forward.  Try to predict what it does. You won't get it first time, but you have 4 attempts, so click again.

When you, eventually, get it correct, then tick it off on your S.N.O.T. sheet. 

.. codelens:: Sequence_1
	      
   print("Twas bryllyg, and the slythy toves")
   print("Did gyre and gymble in the wabe:")
   print("All mimsy were the borogoves;")
   print("And the mome raths outgrabe.")

Sequences (Task 2)
------------------

.. mchoice:: sequence_2
   :answer_a: 1
   :answer_b: 2
   :answer_c: 3
   :answer_d: 4
   :answer_e: 5
   :correct: c
   :feedback_a: what is 2+1?
   :feedback_b: what is 2+1?
   :feedback_c: yes 2+1=3.
   :feedback_d: what is 2+1?
   :feedback_e: There is no line 5.

   What line is run after line 2?

So far we could do that ourselves much easier. So let us look at something different. We will come back to print when we can do something interesting with it.

Sequences (Task 3)
------------------

.. image:: ../_static/short-sequence.png

Above is some scratch-like code. It makes a turtle move around, with a pen.
Pretend to be a turtle, to help you work it out.

.. mchoice:: sequence_3
   :answer_a: A triangle
   :answer_b: A square
   :answer_c: A pentagon
   :answer_d: A hexagon
   :correct: b
   :feedback_a: How many corners?
   :feedback_b: Yes, it has 4 side, and 4 corners of 90 degrees.
   :feedback_c: How many sides?
   :feedback_d: What is the angle?
 
   What will it draw?

Sequences (Task 4)
------------------
Unfortunately the designers of python turtle, did not choose such good names: ``left`` = ``turn_left``, ``forward`` = ``move_forward`` (I don't think they understand how to use verbs).
   
.. parsonsprob:: sequence_4

   Rearrange to make a program that draws a square.
   -----
   import turtle
   turtle.forward(50)
   =====
   turtle.left(90)
   turtle.forward(50)
   turtle.left(90)
   =====
   turtle.forward(50)
   turtle.left(90)
   turtle.forward(50)
   turtle.left(90)

Sequences (Task 5)
------------------
Below I have started a program.   
Finish it, so that it draws a square. Use what you have already done to help you:

* Run the program.
* Repeat until done:
   
  * Add one or at most two lines.
  * Run it again, did it do what you expected? If not then fix it.
  

.. activecode:: sequence_5
   :nocodelens:

   import turtle
   turtle.forward(50)
   turtle.left(90)

Sequences (Task 6)
------------------

|external-angle|

The turtle moves from B to C, if it does not turn it will head of to D. It must turn toward A.

.. qnum::
   :start: 6

.. mchoice:: sequence_6
   :answer_a: a
   :answer_b: b
   :answer_c: c
   :answer_d: d
   :correct: d
   :feedback_a: The turtle is at point C.
   :feedback_b: The turtle is at point C.
   :feedback_c: The turtle is facing D, and needs to face A. What angle is between them.
   :feedback_d: Yes it is the external angle d.		

   What angle does it need to turn?


.. |triangle| image:: ../_static/turtle-triangle.png
.. |external-angle| image:: ../_static/external-angle.png		      

Sequences (Task 7)
------------------
			    
.. qnum::
   :start: 7

Now do it again, but this time make the turtle draw a triangle. (I started it, but I may have made a mistake). When done it should look like this |triangle|
			    
An algorithm to work out the angle:

 #. Play turtle, to come up with an estimate (is it more or less than 90°?).
 #. Try it.
 #. Ask is it too big or too small.
 #. Then come up with a new estimate, and try that.
 #. Keep going until you find the correct value.

An algorithm for choosing the next estimate:

    If you have two values, one that is too big, and one that is too small, then the best guess for the next value is half way between them. After trying it, this new value will ether be too big or too small or just right. If too big or too small, then you have made the problem ½ as big.


.. activecode:: sequence_7
   :nocodelens:

   import turtle
   turtle.forward(50)
   turtle.left(90)

Next
----

Using only sequences, can get tedious. We have to repeat the instructions, over and over, and if you find a problem, you will have to fix it in many places.

Press back on your browser, and choose the next sheet/page. Also collect a new S.N.O.T. sheet from the tray. Show your progress on the S.N.O.T. sheet.
   









  
