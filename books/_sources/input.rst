==========
Input
==========

.. activecode:: input_1

    name=input("What is your name?")
    print("Hello " +name)

.. activecode:: input_2

    number=input("Tell me a number?")
    print("Your number is " +number)


However
    
.. activecode:: input_3

    number=int(input("Tell me a number?"))
    print("Your number is " + str(number))
    print("Your number times 2 is " +str(number*2))

* What happens if you input a non-number on the 3rd program?
* What does this next one do?

.. activecode:: input_4

    number=input("Tell me a number?")
    print("Your number is " + number)
    print("Your number times 2 is " + number*2)
