============================================================
Arithmetic
============================================================
.. raw:: html
	 
   <style>
   table, th, td {
      border: 1px solid lightgrey;
      border-bottom: 1px solid lightgrey;
      text-align:center;
   }
   </style>

.. qnum::
   :prefix: Arithmatic-1.
   :start: 1

Big question
------------
* How do I got the computer to do the arithmetic for me?

Key words
---------
* Add
* Subtract
* Multiply
* Divide
* Division Remainder (also called modulo).   

Introduction (Task 1)
-------------------------
	   
In arithmetic class, you are asked to do calculations such as “What is 6×7?”.
In computing class, you will never be asked to do this.

.. image:: ../_static/rectangle.png

If I ask “What is the area of the rectangle?”, then don't tell me a number. Use the computer to do the multiplication.

.. mchoice:: arithmatic_m1.1
   :answer_a: 42cm²
   :answer_b: 36cm²
   :answer_c: 6×7cm²
   :answer_d: 6×7cm
   :answer_e: 6×8cm²
   :correct: c
   :feedback_a: I told you not to do the multiplication, that is the computers job.
   :feedback_b: No, that is not correct, the computer is much better than us at multiplying and does not make mistakes.
   :feedback_c: Yes, we can then get the computer to do the rest.
   :feedback_d: No, the units are wrong. What is cm×cm?
   :feedback_e: No, look at the image again, what are the numbers?
 
   So what is the area of the rectangle?

Arithmetic (Task 2)
--------------------
   
Here are some other arithmetic operators.

+----------+-------------------+------------+----------+---------+
| Operator | Meaning           | Example               | Result  |
+----------+-------------------+------------+----------+---------+
|          |                   | Math       | Python   |         |
+==========+===================+============+==========+=========+
| \+       | add               | 4 + 6      |  4 + 6   |  12     |
+----------+-------------------+------------+----------+---------+
| \-       | subtract          | 9 - 3      | 9 - 3    | 6       |
+----------+-------------------+------------+----------+---------+
| \*       | multiply          | 4 × 9      | 4 \* 9   | 36      |
+----------+-------------------+------------+----------+---------+
| \/       | divide            | 14 ÷ 5     | 14 \/ 5  | 2.8     |
+----------+-------------------+------------+----------+---------+
| \/\/     | integer divide    | 14 ÷ 5     | 14 \/\/ 5| 2       |
+----------+-------------------+------------+----------+---------+
| %        | division remainder|            | 14 % 5   | 4       |
+----------+-------------------+------------+----------+---------+
| \*\*     | exponentiation    | 4²         | 4**2     | 16      |
+----------+-------------------+------------+----------+---------+

As you can see, it is mostly the same as maths, except that we use ``*`` for multiply, ``/`` for division (sometimes this is used in maths), ``**`` for exponentiation, and ``%`` for remainder. We also have two types of division.

.. dragndrop:: arithmetic_m1.2
    :feedback: Have you done the ones that you know? Have you discussed with another pair?
    :match_1: 3+4|||Three plus four is
    :match_2: 7-2|||Seven minus two is
    :match_3: 4*5|||Four multiplied by five is
    :match_4: 360/3|||Three hundred and sixty divided by 3 is
    :match_5: 5**2|||five squared is

    maths-2.2 Drag and match.


Now to try it out (Task 3)
--------------------------

To see the answers in python, you will need to print them. In the old days this would send them to the printer, but now we have video display screens instead of printers.

#. Press Run, to see what happens.
#. Add your own, and run again.
#. Add more, play, have fun.
#. Can you use more than one symbol?

.. activecode:: arithmetic_m1.3
   :nocodelens:

   print (3+4)
   print (7-2)
   #Add your own code below here.
   #Lines starting with a hash are ignored. They are called comments, as you can leave comments.

.. qnum::
   :start: 4

Bidmas (Task 4)
---------------
   
.. mchoice:: arithmetic_m1.4
   :answer_a: 5
   :answer_b: 6
   :answer_c: 7
   :answer_d: 8
   :correct: b
   :feedback_a: No, remember bidmas
   :feedback_b: Yes, python uses bidmas. Now try it.
   :feedback_c: No, remember bidmas		
   :feedback_d: No, remember bidmas, multiplication is done before addition.

   What is 2+2*2?

Bidmas (Task 5)
---------------

.. mchoice:: arithmetic_m1.4
   :answer_a: 2+(2*2)
   :answer_b: 2*2+2
   :answer_c: (2+2)*2
   :answer_d: (2+2*2)
   :correct: c
   :feedback_a: No, remember bidmas: brackets, indices, multiply or divide, add or subtract 
   :feedback_b: No, remember bidmas: brackets, indices, multiply or divide, add or subtract 
   :feedback_c: Yes, “we will always have bidmas”. Now try it.		
   :feedback_d: No, remember bidmas: brackets, indices, multiply or divide, add or subtract 

   What do we do if we want the addition done first?

As well as printing the result, you can use it. See these examples.

Use with turtle (Task 6)
-------------------------

.. activecode:: arithmetic_m1.5
   :nocodelens:

   import turtle
   turtle.forward(10*5)

Use with turtle (Task 7)
-------------------------
   
.. activecode:: arithmetic_m1.6
   :nocodelens:

   import turtle
   for i in range(3):
       turtle.forward(50)
       turtle.left(360/3)
