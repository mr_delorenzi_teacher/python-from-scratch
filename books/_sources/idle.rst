===================================================
Writing your own programs using IDLE
===================================================

.. qnum::
   :prefix: Idle.
   :start: 1

We have done some python, now it is time to use it outside of this book.
We will return to using the book again later.

Big Question
------------
* What is idle?
* How do I use it?

What is IDLE? (1)
-----------------

Idle is
 * a text editor: you can type in code, and save it, but you can not format the text (like in word).
 * a place to run your python code. After typing in python code. You can run it.

Discuss with a partner, what you think this means.

Starting Python IDLE (2)
-------------------------------
Idle is a program on you computer. If you are using Microsoft's Windows 10 then follow the steps bellow.

.. |start-button|  image:: ../_static/idle/ms-start.png
.. |start-menu|    image:: ../_static/idle/start-idle.png
.. |idle-shell|    image:: ../_static/idle/idle-shell.png
.. |file-newfile|  image:: ../_static/idle/idle-newfile.png
.. |editor-window| image:: ../_static/idle/idle-editor-window.png
			  
First click the start menu |start-button| then click ``Python IDLE`` it is under P for Python.

|start-menu|

A window like this will appear (but a bit bigger).

|idle-shell|

Open a new file (3).
--------------------
This is where you will type your work.

Go to file → newfile |file-newfile|

A new window will appear. |editor-window|

Type some code into python (4)
------------------------------

.. |editor-prog| image:: ../_static/idle/idle-editor-with-program.png
.. |f5|          image:: ../_static/f5.png
.. |save|        image:: ../_static/idle/idle-save.png

Type a program into this new window. In the picture I have started a program.
Don't write more than these 3 lines before running the program.

|editor-prog|

Save the program (5)
--------------------

First save the program, insure that you give it a good name. |save|

Run your program (6)
--------------------

To run the program press |F5|

Practice (7)
------------

Now try writing a small program (such as one to draw a square). Run it several times, by pressing F5,  as you are writing it. You should find the process similar to what you have already done in this book. But now you can save your work on your computer.

Load a program into IDLE (8)
----------------------------

It is ok saving stuff, but if you can not load it again letter, then it will be for nothing.

To load a program into IDLE:
 * In file-explorer, right click on the file.
 * Select Edit in Idle.
