======================
Iteration Challenges
======================

Introduction
------------

Do these challenges in the Python IDLE editor. 
Click here → `Writing your own programs using IDLE`__ for instructions on how to do this. 

.. __: idle.html

Big Question
------------
Can I write my own programs?

Keywords
--------
Iteration, editor, program

Success criteria
----------------
Each of these can be done in 4 lines of code, and one is ``import turtle``.
Do each in a separate file, no more than 4 lines per program.

Polygons
--------

**In only 4 lines of code (each)**. Only the 1st need be 4 lines, the others can be 3 lines, as you only have to do ``import turtle`` once.

  If for each shape we end up where we start, and facing the same way. Then how many times do we turn? What is that in degrees?
  
  So for a square we do that in 4 chunks. That is ``360/4``. What would it be for a triangle, for a hexagon …

.. |triangle| image:: ../_static/turtle-triangle.png
.. |square| image:: ../_static/turtle-square.png
.. |20-gon| image:: ../_static/20-gon.png
.. |poly-code| image:: ../_static/polygon-code.png

|poly-code|  |triangle|  |square|  |20-gon|  

* Draw a square (task1)
* Draw a triangle (task2)
* Draw a hexagon (task3)
* Draw a 20-gon (In geometry, an icosagon or 20-gon is a twenty-sided polygon) (task4)

**Remember only 3 lines of code each +** ``import turtle``

..

Other shapes (Task 5)
---------------------
  
  What would happen if instead of turning 360°, we turned 2×360°? (in python that is ``2*360``).

Try some shapes with 5,7,8,9 … sides where the turtle turns 2×360°, or 3×360° or …

How do you need to change ``360/n`` where ``n`` is the number of sides.

.. |star-2-5| image:: ../_static/star-2-5.png
.. |star-2-7| image:: ../_static/star-2-7.png
.. |star-3-7| image:: ../_static/star-3-7.png		      

|star-2-5|  |star-2-7|  |star-3-7|

Spiral (Task 6)
---------------

Predict what will happen when you click forward. You won't get it first time, so click again.

.. codelens:: iteration variable

   for i in range(5):
       print(i, i*2)
       
Spiral (Task 6.5)
-----------------

How can you use this code to draw a spiral? ``turtle.forward(2*i)``

Multiple (Task 7)
-----------------

.. |polygons| image:: ../_static/polygons.png

|polygons|

Shot teacher what you have done (Task 8)
----------------------------------------
