======================
Iteration
======================

.. qnum::
   :prefix: Iteration.
   :start: 1

We have done some python, and we have used iteration (loops). Working in pairs, do the following exercises.

Big Question
------------
* What is iteration?
* How can I use iteration to make my code smaller?

Keywords
--------
* Iteration
* Loop
* Indent
* ``for i in range(n):`` where n is a number.

Iteration (Task 1)
------------------

.. mchoice:: iteration_2.1
   :answer_a: To add up all of the values in a list.
   :answer_b: It will make it easier to change the code later.
   :answer_c: To make a decision.
   :answer_d: I will not have to type as much.      
   :answer_e: To calculate the value of an angle.
   :correct: d,b
   :feedback_a: No, while it could be used in a program that does that, its job is not calculation.
   :feedback_b: Yes, because there is less code, it will be easier to change.
   :feedback_c: No, you would use a selection (if), to do that (we have not done them yet).
   :feedback_d: Yes, because there is less code, you will not have to type as much.
   :feedback_e: No, it is not used to calculate.

   Click to two correct answers. Why would you use a loop, in your code?

Iteration (Task 2)
------------------

.. mchoice:: iteration_2.2
   :answer_a: The program stops.
   :answer_b: It runs the line of code after the loop.
   :answer_c: It starts the program again.
   :answer_d: It repeats the loop.
   :correct: b
   :feedback_a: No, the program stops when there is no more code to run.
   :feedback_b: Yes, it does what ever is next.
   :feedback_c: No, it would only start again, if you tell it to.
   :feedback_d: No, read the question carefully. It has finished looping, so it has already done this.

   What does the program do when it finishes looping?

Iteration (Task 3)
------------------
   
Let us now look at a program. This one produces instructions for a person.
The first version is in scratch.

.. image:: ../_static/person-draw-triangle-scratch.png

Then this version is in a sort of pythony scratch. It looks like scratch, but is python.
	   
.. image:: ../_static/person-draw-triangle.png
	   
Now in python.
Run the program one line at a time, by pressing forward.
   
.. codelens:: iteration_2_3
   :question: What line will be executed after the current line executes?
   :feedback: Line 1 tells us to run the two indented lines 3 times.
   :breakline: 5
   :correct: line
	      
   for i in range(3):
       print("Person move forward 50 steps.")
       print("Person turn left 120 degrees.")
   print("Person stop.")

Iteration (Task 4)
------------------
   
.. qnum::
   :start: 4
   
.. mchoice:: iteration_2.4
   :answer_a: 1.
   :answer_b: 2.
   :answer_c: 3.
   :answer_d: 4.
   :correct: c
   :feedback_a: No, what is the number in the line <pre>for i in range(…):</pre>
   :feedback_b: No, what is the number in the line <pre>for i in range(…):</pre>
   :feedback_c: Yes, because we have the number 4 in the line <pre>for i in range(4):</pre>
   :feedback_d: No, what is the number in the line <pre>for i in range(…):</pre>

   How many times did the above code loop?

Iteration (Task 5)
------------------
   
.. parsonsprob:: iteration_2.5

   Drag the blocks to the right, to make a program that draws a square, then draws a triangle.
   -----
   import turtle
   =====
   for i in range (4):
   =====
       turtle.forward(50)
       turtle.left(90)
   =====
   draw_triangle()

Iteration (Task 6)
------------------

.. mchoice:: iteration_2.6
   :answer_a: Because they are turtle commands.
   :answer_b: Because there is two of them.
   :answer_c: Because these are the commands to be repeated.
   :answer_d: Because squares have 4 sides. 
   :correct: c
   :feedback_a: No, look again. Which commands are run multiple times.
   :feedback_b: No, there can be one or more commands, that are indented.
   :feedback_c: Yes, we repeat the indented commands.
   :feedback_d: No, the number in <pre>for i in range(?):</pre>, and the angle determine the shape.

   Why did two lines of code need to be indented (moved away from the left edge)?

Iteration (Task 7)
------------------
   
.. dragndrop:: iteration_2.7
    :feedback: Have you done the ones that you know? Have you discussed with another pair?
    :match_1: indented.|||The repeated code is
    :match_2: the line after the indentation is run.|||After the loop is finished
    :match_3: ends with a colon:|||The line <pre>for i in …</pre>
    :match_4: the space at the beginning of the line.|||Indentation is	      

    Iteration-2.7 Drag the sentence endings to the sentence starts on the right.

Iteration (Task 8)
------------------
    
.. qnum::
   :start: 8

.. parsonsprob:: iteration_2.8

   Rearrange to make a program that draws a triangle.
   -----
   import turtle
   =====
   for i in range (3):
   =====
       turtle.forward(50)
       turtle.left(120)

Iteration (Task 9)
------------------
 
.. parsonsprob:: iteration_2.9

   Rearrange to make a program that draws a triangle, then a square.
   -----
   import turtle
   =====
   for i in range (3):
   =====
       turtle.forward(50)
       turtle.left(120)
   =====
   for i in range (4):
   =====
       turtle.forward(50)
       turtle.left(90)

Iteration (Task 10)
-------------------
       
.. parsonsprob:: iteration_2.10

   Rearrange to make a program that draws 4 triangles
   -----
   import turtle
   =====
   for i in range (4):
   =====
       for i in range (3):
   =====
           turtle.forward(50)
           turtle.left(120)
   =====
       turtle.left(90)

Iteration extra
---------------
       
Use the following area to experiment, try it out. I have started some code for you. What can you add or change? Do not do more than 10 lines (you can not save).
       
.. activecode:: iteration_2_extra
   :nocodelens:
		
   import turtle
   for i in range (3):
      turtle.forward(50)
      turtle.left(120)
