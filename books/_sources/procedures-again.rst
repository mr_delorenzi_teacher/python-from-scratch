======================
Procedures again
======================

.. qnum::
   :prefix: Procedures-2.
   :start: 1

We have already created procedures. Now to create procedures that we can pass data to.

Big Question
------------
* How can I pass data to a procedure?

Key words
---------
* Procedures
* Parameters

Introduction
------------

So far we have procedures like ``draw_square()``. However we have seen procedures such as ``turtle.forward(100)``, so how do we make it so that we can pass data in such as the ``100`` above. Read on to find out.

Procedures again (Task 1)
-------------------------

Remember predict, then click run.

.. activecode:: procedures_2_1
   :nocodelens:
		
   def say_greeting():
      print ("Hello")
	 
   say_greeting()
   say_greeting()

Procedures again (Task 2)
-------------------------

Now we want to tell it who to greet. For example ``say_greeting("Mr D")``, should output ``Hello Mr D``.

.. activecode:: procedures_2_2
   :nocodelens:
		
   def say_greeting(name):
      print ("Hello " +name)
	 
   say_greeting("Mr D")
   say_greeting("Student")

**Now make some changes, and run again. Experiment, play, learn.**

Procedures again (Task 3)
-------------------------

Now with turtles

.. activecode:: procedures-2.3
   :nocodelens:
      
   import turtle
   def draw_triangle(size):
      for i in range (3):
         turtle.forward(size)
         turtle.left(120)
	 
   draw_triangle(10)
   draw_triangle(20)
   draw_triangle(30)

**Now make some changes, and run again. Experiment, play, learn.**

Procedures again (Task 4)
--------------------------

Or with a loop.

.. activecode:: procedures-2.4
   :nocodelens:
      
   import turtle
   def draw_triangle(size):
      for i in range (3):
         turtle.forward(size)
         turtle.left(120)

   for i in range(3):
      print (i)
      draw_triangle(10*i)

**What went wrong?**
**Now make some changes, and run again. Experiment, play, learn.**

Procedures again (Task 5)
-------------------------

.. mchoice:: sequence_3
   :answer_a: Tell the procedure the angle.
   :answer_b: Tell the procedure the size.
   :answer_c: Tell the procedure how many times to loop.
   :answer_d: Tell the procedure the shape.
   :correct: b
   :feedback_a:  No it draws triangles, the angle is always 120°.
   :feedback_b: Yes, the number goes to turtle.forward, to tell it how far to move.
   :feedback_c: No it draws triangles, always with 3 sides.
   :feedback_d: No this procedure only does triangles
 
   In the code above, what does the number, that we pass to draw_triangle do?

Procedures again (Task 6)
-------------------------

Now to try a polygon procedure.

.. parsonsprob:: procedures_2_6

   Rearrange to make it draw some polygons 
   -----
   import turtle

   def draw_polygon(number_of_sides):
   =====
       for i in range(number_of_sides):
           turtle.forward(50)
           turtle.right(360/number_of_sides)
   =====
   for n in range(3,10):
       draw_polygon(n)


Procedures again (Task 7)
-------------------------

Now try it, and make some changes. Experiment, play, learn.

.. reveal:: revealid1
    :showtitle: Reveal Content
    :hidetitle: Hide Content
		
    .. activecode:: procedures-2_7
       :nocodelens:
      
       import turtle

       def draw_polygon(number_of_sides):
  
	  for i in range(number_of_sides):
	      turtle.forward(50)
              turtle.right(360/number_of_sides)

       for n in range(3,10):
	  draw_polygon(n)

Procedures again (Task 8)
-------------------------

Fix this one. It should draw the polygons approximately the same size.
But I forgot one thing. Where does the variable ``size`` go, in the procedure?

Clue look at ``number_of_sides``. It appears 3 times, where? ``size`` should appear twice, where?

.. reveal:: revealid1
    :showtitle: Reveal Content
    :hidetitle: Hide Content
		
    .. activecode:: procedures-2_8
       :nocodelens:
      
       import turtle

       def draw_polygon(number_of_sides, size):
  
	  for i in range(number_of_sides):
	      turtle.forward(50)
              turtle.right(360/number_of_sides)

       for n in range(3,10):
	  draw_polygon(n,200/n)
   

