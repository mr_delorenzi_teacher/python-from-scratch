======================
Procedures
======================

.. qnum::
   :prefix: Procedures-1.
   :start: 1

We have done some python, now for procedures

Big Question
------------
* What are procedures?
* How can I use procedures to make my code easier to read?
* How can I use procedures to make my code simpler?

Key words
---------
* Procedure
* Indentation
* ``def``


Procedures (Task 1)
-------------------
Read the text below, then discuss with a partner, then click the correct answers.

    A procedure is a set of instructions with a name. Like turn on the light, or turtle.forward.

    Some times we want to make our own procedures. May be to draw a square or triangle. After defining a new procedure to draw a square we can just say ``draw_square()``, or ``draw_square(100)``, in the 2nd example the 100 could tell it how big to draw the square.

.. mchoice:: procedures_2.1
   :answer_a: It allows code reuse.
   :answer_b: It will make it easier to change the code later.
   :answer_c: To make a decision.
   :answer_d: I will not have to type as much.      
   :answer_e: To calculate the value of an angle.
   :correct: a,d,b
   :feedback_a: Yes, this is the main reason to use procedures.
   :feedback_b: Yes, because there is less code, it will be easier to change.
   :feedback_c: No, you would use a selection (if), to do that (we have not done them yet).
   :feedback_d: Yes, because there is less code, you will not have to type as much.
   :feedback_e: No, it is not used to calculate.

   Why would you use a procedure (def), in your code?

Procedures (Task 2)
-------------------
   
.. mchoice:: procedures_2.2
   :answer_a: The program stops.
   :answer_b: It runs the line of code after the definition.
   :answer_c: It starts the program again.
   :answer_d: It runs the procedure.
   :correct: b
   :feedback_a: No, the program stops when there is no more code to run.
   :feedback_b: Yes, it does what ever is next.
   :feedback_c: No, it would only start again, if you tell it to.
   :feedback_d: No, a def only defines a procedure, it does not yet run it.

   What does the program do when it finishes defining a procedure (def)?

Procedures (Task 3)
-------------------
   
Let us now look at a program. Looks like some sort of poem.
The first version is in scratch.

.. image:: ../_static/procedure-scratch.png

Then this version is in a sort of pythony scratch. It looks like scratch, but is python.
	   
.. image:: ../_static/procedure-definition.png
	   
Now in python.
Run the program one line at a time, by pressing forward. Don't forget to predict what it will do, before each time you press forward. Remember I don't expect you to get it first time.
   
.. codelens:: procedures_2_3
   :question: What line will be executed after the current line executes?
   :feedback: Line 1 tells us to run the two indented lines 3 times.
   :breakline: 9
   :correct: line
	      
   def verse4():
       print("With fingers pale and trembling, slowly toward the keyboard bending,")
       print("Longing for a happy ending, hoping all would be restored,")
       print("Praying for some guarantee, timidly, I pressed a key.")
       print("But on the screen there still persisted words appearing as before.")
       print("Ghastly grim they blinked and taunted, haunted, as my patience wore,")
       print("Saying “Abort, Retry, Ignore?”")
       
   verse4()
   print ("")
   verse4()


Procedures (Task 4)
-------------------
   
.. qnum::
   :start: 4
   
.. mchoice:: procedures_2.4
   :answer_a: 1.
   :answer_b: 2.
   :answer_c: 3.
   :answer_d: 4.
   :correct: a
   :feedback_a: Yes, we defined it one, and used it twice
   :feedback_b: No, we used it twice, but how many times did we define it.
   :feedback_c: No, how many times do you see <pre>def verse4():</pre>
   :feedback_d: No, how many times do you see <pre>def verse4():</pre>

   How many times did we define the procedure verse4?

Procedures (Task 5)
-------------------
   
.. parsonsprob:: procedures_2.5

   Drag the blocks to the right, to make a program that draws a square, turns, and draws another square.
   -----
   import turtle
   =====
   def draw_square():
   =====
       for i in range (4):
   =====
           turtle.forward(50)
           turtle.left(90)
   =====
   draw_square()
   turtle.left(45)
   =====
   draw_square()

.. image:: ../_static/rusian-dolls.jpeg

Procedures (Task 6)
-------------------
	   
.. mchoice:: procedures_2.6
   :answer_a: Because they are turtle commands.
   :answer_b: Because these are the commands in the procedure definition (def).
   :answer_c: Because these are the commands to be repeated.
   :answer_d: Because they are in the loop that is in the procedure definition. 
   :correct: d
   :feedback_a: There is another turtle command that is not indented, so it can not be for this reason.
   :feedback_b: They are in a procedure definition, so this explains one level of indentation.
   :feedback_c: They are in a loop, so this explains one level of indentation.
   :feedback_d: Yes, we can put things in things in things.

   In the above code, two of the lines are indented twice. Why?

Procedures (Task 7)
-------------------
   
.. dragndrop:: procedures_2.7
    :feedback: Have you done the ones that you know? Have you discussed with another pair?
    :match_1: indented.|||The code belonging to the defined procedure is
    :match_2: Remembers the indented code, and gives it the name that follows def, but does not run the code.|||When the python interpreter gets to a line starting def it
    :match_3: ends with a colon:|||The line <pre>def … ()</pre>
    :match_4: the space at the beginning of the line.|||Indentation is	      

    Procedures-2.7 Drag the sentence endings to the sentence starts on the right.

Procedures (Task 8)
-------------------
    
.. qnum::
   :start: 8

.. parsonsprob:: procedure_2.8

   Rearrange to make a program that draws a triangle.
   -----
   import turtle
   =====
   def draw_triangle():
   =====
       for i in range (3):
   =====
           turtle.forward(50)
           turtle.left(120)
   =====
   draw_triangle()

Procedures (Task 9)
-------------------
   
.. parsonsprob:: iteration_2.9

   Rearrange to make a program that draws a triangle, then a square.
   -----
   import turtle
   def draw_triangle():
   =====
      for i in range (3):
   =====
         turtle.forward(50)
         turtle.left(120)
   =====
   def draw_square():
   =====
      for i in range (4):
   =====
         turtle.forward(50)
         turtle.left(90)   
   =====
   draw_square()
   turtle.left(90)
   turtle.forward(50)
   turtle.right(90)
   draw_triangle()

Procedures (Task 10)
--------------------

Now click Reveal code, and try it out.

.. reveal:: reveal_iteration_2_9b
   :showtitle: Reveal Code
   :hidetitle: Hide Code

   .. activecode:: iteration_2_9b
      :nocodelens:		
		
      import turtle
   
      def draw_triangle():
	 for i in range (3):
	    turtle.forward(50)
	    turtle.left(120)

      def draw_square():
	 for i in range (4):
	    turtle.forward(50)
	    turtle.left(90)   
  
      draw_square()
      turtle.left(90)
      turtle.forward(50)
      turtle.right(90)
      draw_triangle()

Procedures (Task 11)
--------------------
      
.. parsonsprob:: procedure_2.11

   Rearrange to make a program that draws 4 triangles
   -----
   import turtle
   =====
   def draw_triangle():
   =====
      for i in range (3):
   =====
         turtle.forward(50)
         turtle.left(120)

   =====
   for i in range(4):
   =====
      draw_triangle()
      turtle.left(90)

Procedures extra
----------------
      
Use the following area to experiment.
       
.. activecode:: iteration_2_extra
		
   import turtle
