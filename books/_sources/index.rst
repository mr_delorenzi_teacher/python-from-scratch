.. raw:: html

   <div style="text-align:center" class="center-block">
   <h1>Python from Scratch</h1>
   <style>
   button.reveal_button {
       margin-left: auto;
       margin-right: auto;
   }
   </style>
   </div>

.. reveal:: about
   :showtitle: About this Project
   :modal:
   :modalTitle: About this Project

   Content Copyright 2018 R. Delorenzi, you may use and copy under the Gnu Free Documentation licence. It uses the Runestone tool kit.

   The Runestone Interactive tools are open source and we encourage you to grab a copy from GitHub if you would like to use them to write your own resources.

Table of Contents
:::::::::::::::::
   
.. toctree::
   :numbered:
   :maxdepth: 1

   sequence.rst
   iteration.rst
   idle.rst
   challenges-iter.rst
   procedures.rst
   math-m.rst
   procedures-again.rst
   challenges-procedures.rst
   input.rst

----

.. image:: https://imgs.xkcd.com/comics/iso_8601.png
   :alt: xkcd explains why the date should be year-month-date



	   
